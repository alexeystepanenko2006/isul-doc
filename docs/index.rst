Welcome to **Prism** developer documentation!
===================================

Check out the :doc:`api` section for further information, including
how to :ref:`installation` the project.

.. note::

   This project is under active development.

Contents
--------

.. toctree::

   api
   
Documentation hosted on Read the Docs.

